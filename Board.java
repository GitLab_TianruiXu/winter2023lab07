
public class Board {
	private Square[][] tictactoeBoard;
	
	public Board() {
		this.tictactoeBoard = new Square[3][3];
		for(int i = 0; i < tictactoeBoard.length; i ++) {
			for(int j = 0; j < tictactoeBoard[i].length; j++) {
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString() {
		String returnString = "  0 1 2\n";
		for(int i = 0; i < tictactoeBoard.length; i ++) {
			returnString += i + " ";
			for(int j = 0; j < tictactoeBoard[i].length; j++) {
				returnString += tictactoeBoard[i][j] + " ";
			}
			returnString += "\n";
		}
		return returnString;
	}
	
	public boolean placeToken(int row, int col, Square playerToken) {
		if(row < 0 || row > tictactoeBoard.length || col < 0 || col > tictactoeBoard[0].length) {
			return false;
		}
		if(this.tictactoeBoard[row][col] == Square.BLANK) {
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}else {
			return false;
		}
	}
	
	public boolean checkIfFull() {
		int countBlank = 0;
		for(int i = 0; i < tictactoeBoard.length; i ++) {
			for(int j = 0; j < tictactoeBoard[i].length; j++) {
				if(tictactoeBoard[i][j] == Square.BLANK) {
					countBlank += 1;
				}
			}
		}
		return countBlank > 0 ? false : true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken) {
		int tokenCount = 0;
		for(int i = 0; i < tictactoeBoard.length; i ++) {
			for(Square token : tictactoeBoard[i]) {
				if(token == playerToken) {
					tokenCount ++;
				}
			}
			if(tokenCount == 3) {
				return true;
			}else {
				tokenCount = 0; //re-initialize tokenCount
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken) {
		int tokenCount = 0;
		for(int i = 0; i < tictactoeBoard[0].length; i ++) {
			for(int j = 0; j < tictactoeBoard.length; j ++) {
				if(tictactoeBoard[j][i] == playerToken) {
					tokenCount ++;
				}
			}
			if(tokenCount == 3) {
				return true;
			}else {
				tokenCount = 0; //re-initialize tokenCount
			}
		}
		return false;
	}
	
	public boolean checkIfWinningDiagonal(Square playerToken) {
		int tokenCount = 0;
		for(int i = 0; i < tictactoeBoard.length; i ++) {
			if(tictactoeBoard[i][i] == playerToken) {
				tokenCount ++;
			}
		}
		if(tokenCount == 3) {
			return true;
		}else {
			tokenCount = 0; //re-initialize tokenCount
		}
		for(int i = tictactoeBoard.length - 1, j = 0; i >= 0; i --, j ++) {
			if(tictactoeBoard[j][i] == playerToken) {
				tokenCount ++;
			}
		}
		if(tokenCount == 3) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean checkIfWinning(Square playerToken) {
		if(checkIfWinningDiagonal(playerToken) || checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
			return true;
		}else {
			return false;
		}
	}
}
