import java.util.Scanner;
public class TicTacToeGame {

	public static void main(String[] args) {
		System.out.println("Welcome to Tic Tac Toe Game");
		Board b1 = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		Scanner userInput = new Scanner(System.in);
		while(!gameOver) {
			//set playerToken
			if(player == 1) {
				playerToken = Square.X;
			}else {
				playerToken = Square.O;
			}
			//get row and col
			System.out.println("Player " + player + ": it's your turn where do you want to place your token?");
			int row = userInput.nextInt();
			int col = userInput.nextInt();
			//place token
			while(!b1.placeToken(row, col, playerToken)) {
				System.out.println("Please enter valid inputs");
				row = userInput.nextInt();
				col = userInput.nextInt();
			}
			//check winner
			if(b1.checkIfWinning(playerToken)) {
				System.out.println("Player " + player + " wins!");
				gameOver = true;
			}
			//check if full
			if(b1.checkIfFull()) {
				System.out.println("It's tie!");
				gameOver = true;
			}else {
				if(player == 1) {
					player = 2;
				}else {
					player = 1;
				}
			}
			System.out.println(b1);
		}
	}
}
